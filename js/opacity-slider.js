import throttle from './throttle';

export default function initSliders(selector = '.opacity-slider', opts = {}) {
  const sliders = document.querySelectorAll(selector);
  return Array.from(sliders).map((root) => createOpacitySlider(root, opts));
}

export function createOpacitySlider(root, { switchCallback = (from, to) => {}, trigger = 'click', delay = 7000, order = 1 } = {}) {
  const slider = {
    delay,
    autoPlay: false,
    activeIndex: -1,
    timeout: null,
    root,
    slides: root.querySelectorAll('.slide')
  }

  if (order === -1) {
    slider.slides = Array.from(slider.slides).reverse();
  }


  slider.play = function() {
    slider.autoplay = true;
    if (slider.activeIndex < 0) {
      slider.switchSlide();
    } else {
      setTimeout(slider.switchSlide, slider.delay);
    }
  },

  slider.pause = function() {
    slider.autoplay = false;
    clearTimeout(slider.timeout);
  },

  slider.switchSlide = function(to = (slider.activeIndex + 1)) {
    if (slider.timeout) {
      clearTimeout(slider.timeout);
      slider.timeout = null;
    }
    const from = slider.activeIndex;
    to = to % slider.slides.length;
    if (from !== to) {
      switchCallback(from, to);
      setTimeout(() => {
        if (from >= 0) {
          slider.slides[from].classList.remove('active');
        }
        slider.activeIndex = to;
        slider.slides[slider.activeIndex].classList.add('active');
      }, 10);
    }
      
    if (slider.autoplay) {
      clearTimeout(slider.timeout);
      slider.timeout = setTimeout(slider.switchSlide, slider.delay);
    }
  }

  if (trigger === 'click') {
    const onClick = throttle(() => slider.switchSlide(), 500);
    slider.root.addEventListener('click', onClick);
  }

  return slider;
}