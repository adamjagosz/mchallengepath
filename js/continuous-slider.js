import { gsap } from "gsap";

export default function initSliders({ delay = 4000, duration = 1200 } = {}) {
    const sliders = document.getElementsByClassName('slider-continuous');
    return Array.from(sliders).map((root) => setupSlider(root, { delay, duration }));
}

function bindAndStop(element, eventName, callback, stop = true) {
    element.addEventListener(eventName, (e) => {
        callback(e);
        if (stop) {
            e.stopPropagation();
            return false;
        }
    });
}

function setupSlider(root, { delay, duration }) {
    var slider = {
        touchstartx: undefined,
        touchmovex: undefined,
        touchstarty: undefined,
        touchmovey: undefined,
        movex: undefined,
        index: 0,
        longTouch: undefined,
        slideSize: 0,
        autoplay: true,
        loopIndex: 0,
    };
    const sliderOffset = root.dataset.sliderOffset || 0.2;
    gsap.set(".slider-item", {
        x: (i) => ((i + sliderOffset) * 100) + '%'
    });
    const slides = Array.from(root.querySelectorAll('.slider-item'));
    const slideCount = slides.length;
    root.style.setProperty('--slideCount', slideCount);

    let autoPlayTimeout;

    function play() {
        slider.autoplay = true;
        if (autoPlayTimeout) clearTimeout(autoPlayTimeout);
        autoPlayTimeout = setTimeout(() => {
            switchSlide(1);
            play();
        }, delay);
    }
    function pause() {
        slider.autoplay = false;
        if (autoPlayTimeout) clearTimeout(autoPlayTimeout);
    }

    function navigateSlider(i) {
        pause();
        switchSlide(i);
        setTimeout(play, delay);
    }

    // const tween = gsap.to(".slider-item", {
    //     duration: 6 * 9,
    //     ease: "none",
    //     x: "-=500%",
    //     modifiers: {
    //     x: gsap.utils.unitize(x => gsap.utils.wrap(-100, 500)(parseFloat(x)), '%')
    //     },
    //     repeat: -1
    // });
    // tween.pause();
    // return tween; 

    // const tween = gsap.to(".slider-item", {
    //     duration: duration / 1000,
    //     ease: "Power3.easeInOut",
    //     x: `-=100%`,
    //     modifiers: {
    //         x: gsap.utils.unitize(x => gsap.utils.wrap(-100, (slideCount - 1) * 100)(parseFloat(x)), '%')
    //     },
    // });


    function switchSlide(i) {
        slider.index = (slider.index + i + slideCount) % slideCount;
        // root.style.setProperty('--sliderIndex', slider.index);
        // root.style.setProperty('--sliderLoopIndex', slider.loopIndex);
        // Array.from(root.querySelectorAll(`.slider_body .slide`))
        //   .forEach(e => e.classList.remove('active'));
        // Array.from(root.querySelectorAll(`.slider_body .slide:nth-child(${slider.index + 1})`))
        //   .forEach(e => e.classList.add('active'));

        const offset = i * 100;
        const tween = gsap.to(".slider-item", {
            duration: duration / 1000,
            ease: "Power2.easeInOut",
            x: `-=${offset}%`,
            modifiers: {
                x: gsap.utils.unitize(x => gsap.utils.wrap(-100, (slideCount - 1) * 100)(parseFloat(x)), '%')
            },
        });
    }

    setupTouchNavigation(root);

    Array.from(root.querySelectorAll('.button_prev')).forEach(b => {
        bindAndStop(b, 'click', () => navigateSlider(-1));
    });
    Array.from(root.querySelectorAll('.button_next')).forEach(b => {
        bindAndStop(b, 'click', () => navigateSlider(1));
    })

    function setupTouchNavigation(touchElement, options = {}) {
        options = Object.assign({ slideAnimation: true }, options); // overwrite defaults

        const slide = root.querySelector('.slider-item');
        slider.slideSize = slide.getBoundingClientRect().width;
        const slideMargin = .33;
        const minPosition = -slideMargin * slider.slideSize;
        const maxPosition = (slideCount - 1 + slideMargin) * slider.slideSize;


        touchElement.addEventListener("touchstart", function(event) {
            slider.longTouch = false;
            setTimeout(function() {
                slider.longTouch = true;
            }, 250);
            slider.touchstartx = event.touches[0].pageX;
            slider.touchstarty = event.touches[0].pageY;
            root.classList.remove('animated');
        },);
        touchElement.addEventListener("touchmove", function(event) {
            slider.touchmovex = event.touches[0].pageX;
            slider.touchmovey = event.touches[0].pageY;
            const dx = slider.touchstartx - slider.touchmovex;
            const dy = slider.touchstarty - slider.touchmovey;
            if (Math.abs(dx) > Math.abs(dy)) {
                slider.movex = slider.index * slider.slideSize + dx;
                if (options.slideAnimation) {
                    const position = Math.max(minPosition, Math.min(maxPosition, slider.movex));
                    const positionPercent = dx / slider.slideSize;
                    console.log(slider.slideSize)
                    gsap.to(".slider-item", {
                        duration: .1,
                        ease: "none",
                        x: `-=${positionPercent}%`,
                        modifiers: {
                            x: gsap.utils.unitize(x => gsap.utils.wrap(-100, (slideCount - 1) * 100)(parseFloat(x)), '%')
                        },
                    });
                }
                event.preventDefault();
            }
        },);
        touchElement.addEventListener("touchend", function(event) {
            root.classList.add('animated');
            const minFlick = .33;
            if (slider.touchmovex > 0) { // prevent navigating on the click to open full screen (??)
                var distance = slider.index * slider.slideSize - slider.movex;
                if (Math.abs(distance) > slider.slideSize * minFlick || !slider.longTouch) {
                    if (distance > 0) {
                        navigateSlider(-1);
                    } else if (distance < 0) {
                        navigateSlider(1);
                    }
                }
            }
            slider.touchmovex = 0;
        },);

    }

    switchSlide(0);
    // play();

    return { root, switchSlide, play, pause };

    // const buttonClose = root.querySelector('.button_close');
    // buttonClose.addEventListener('click',showHideFullscreen); // disable if close on bodyFull
}
