export default function initSliders({ delay = 4000 } = {}) {
    const sliders = document.getElementsByClassName('slide-slider');
    return Array.from(sliders).map((root) => setupSlider(root, { delay }));
}

function bindAndStop(element, eventName, callback, stop = true) {
    element.addEventListener(eventName, (e) => {
        callback(e);
        if (stop) {
            e.stopPropagation();
            return false;
        }
    });
}

function setupSlider(root, { delay }) {
    var slider = {
        touchstartx: undefined,
        touchmovex: undefined,
        touchstarty: undefined,
        touchmovey: undefined,
        movex: undefined,
        index: 0,
        longTouch: undefined,
        slideSize: 0,
        autoplay: true,
        loopIndex: 0,
    };
    const maxSlides = +(root.dataset['sliderSize'] || root.querySelector('.slider_body').children.length);
    const slidesPerPage = +(root.dataset['slidesPerPage'] || 1);
    const sliderLoop = !!(root.dataset['sliderLoop']);
    if (sliderLoop) {
        root.classList.remove('first_slide');
        root.classList.remove('last_slide');
    }
    root.style.setProperty('--slideCount', maxSlides);

    let autoPlayTimeout;

    function play() {
        slider.autoplay = true;
        if (autoPlayTimeout) clearTimeout(autoPlayTimeout);
        autoPlayTimeout = setTimeout(() => {
            switchSlide(1);
            play();
        }, delay);
    }
    function pause() {
        slider.autoplay = false;
        if (autoPlayTimeout) clearTimeout(autoPlayTimeout);
    }

    function navigateSlider(i) {
        pause();
        switchSlide(i);
        setTimeout(play, 6000);
    }

    function switchSlide(i) {
        slider.index = (slider.index + i + maxSlides) % maxSlides;

        if (!sliderLoop) {
          root.classList.toggle('first_slide', slider.index === 0);
          root.classList.toggle('last_slide', slider.index === maxSlides - 1);
        }
        root.style.setProperty('--sliderIndex', slider.index);
        root.style.setProperty('--sliderLoopIndex', slider.loopIndex);
        Array.from(root.querySelectorAll(`.slider_body .slide`))
          .forEach(e => e.classList.remove('active'));
        Array.from(root.querySelectorAll(`.slider_body .slide:nth-child(${slider.index + 1})`))
          .forEach(e => e.classList.add('active'));
    }

    function showHideFullscreen(e) {
        root.classList.toggle('fullscreen');
    }

    const body = root.querySelector('.slider_small');
    const fullscreenRoot = root.querySelector('.slider_fullscreen');
    const fullscreenWrapper = root.querySelector('.slider_fullscreen .slider_wrapper');
    const fullscreenBody = root.querySelector('.slider_fullscreen .slider_body');

    bindAndStop(body, 'click', showHideFullscreen);
    bindAndStop(fullscreenRoot, 'click', showHideFullscreen);
    setupTouchNavigation(body);
    setupTouchNavigation(fullscreenWrapper, fullscreenBody, { slideAnimation: true });

    Array.from(root.querySelectorAll('.button_prev')).forEach(b => {
        bindAndStop(b, 'click', () => navigateSlider(-1));
    });
    Array.from(root.querySelectorAll('.button_next')).forEach(b => {
        bindAndStop(b, 'click', () => navigateSlider(1));
    })

    function setupTouchNavigation(touchElement, transformElement = touchElement, options = {}) {
        options = Object.assign({ slideAnimation: true }, options); // overwrite defaults

        const slide = transformElement.querySelector('.slide');
        slider.slideSize = slide.getBoundingClientRect().width;
        const slideMargin = .33;
        const minPosition = -slideMargin * slider.slideSize;
        const maxPosition = (maxSlides - 1 + slideMargin) * slider.slideSize;


        touchElement.addEventListener("touchstart", function(event) {
            slider.longTouch = false;
            setTimeout(function() {
                slider.longTouch = true;
            }, 250);
            slider.touchstartx = event.touches[0].pageX;
            slider.touchstarty = event.touches[0].pageY;
            root.classList.remove('animated');
        },);
        touchElement.addEventListener("touchmove", function(event) {
            slider.touchmovex = event.touches[0].pageX;
            slider.touchmovey = event.touches[0].pageY;
            const dx = slider.touchstartx - slider.touchmovex;
            const dy = slider.touchstarty - slider.touchmovey;
            if (Math.abs(dx) > Math.abs(dy)) {
                slider.movex = slider.index * slider.slideSize + dx;
                if (options.slideAnimation) {
                    const position = Math.max(minPosition, Math.min(maxPosition, slider.movex));
                    transformElement.style.transform = `translateX(${-position}px)`;
                }
                event.preventDefault();
            }
        },);
        touchElement.addEventListener("touchend", function(event) {
            root.classList.add('animated');
            const minFlick = .33;
            if (slider.touchmovex > 0) { // prevent navigating on the click to open full screen (??)
                var distance = slider.index * slider.slideSize - slider.movex;
                if (Math.abs(distance) > slider.slideSize * minFlick || !slider.longTouch) {
                    if (distance > 0) {
                        navigateSlider(-1);
                    } else if (distance < 0) {
                        navigateSlider(1);
                    }
                }
            }
            transformElement.style.transform = ``;
            slider.touchmovex = 0;
        },);

    }

    switchSlide(0);
    // play();

    return { root, switchSlide, play, pause };

    // const buttonClose = root.querySelector('.button_close');
    // buttonClose.addEventListener('click',showHideFullscreen); // disable if close on bodyFull
}
