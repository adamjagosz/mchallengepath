import onVisible from './visible';
import { onVisibleInvisible } from './visible';
import initPrevs from './prevs';
// import setupSliders from './continuous-slider';
import float from './float';

// import { gsap } from "gsap";
// import { ScrollTrigger } from "gsap/ScrollTrigger";

// gsap.registerPlugin(ScrollTrigger);

$(document).ready(function() {

  initHeader();

  const baubles = document.querySelectorAll('section.prizes .box');
  baubles.forEach(b => {
    onVisibleInvisible(b,
    () => {
      b.classList.add('animate-in');
    },
    () => {
      b.classList.remove('animate-in');
    },
    { condition: r => (vh() - r.top) / vh() > -0.1 && r.bottom / vh() > .1, once: true, ignoreInitial: true });
  });


  document.querySelectorAll('.accordion').forEach(a => {
    const headers = a.querySelectorAll('.card-header');
    a.addEventListener('click', (e) => {
      if (e.target.classList.contains('card-header')) {
        headers.forEach(h => {
          h.classList.remove('active');
        })
        e.target.classList.add('active');
      }
    })
  })


  const graphic = document.querySelector('.graphic-1');
  onVisibleInvisible(graphic, () => graphic.classList.add('animate-in'), () => graphic.classList.remove('animate-in'), 
    { condition: r => (vh() - r.top) / vh() > -0.1 && r.bottom / vh() > 0.2, once: false, ignoreInitial: true }
  );


  // float('.tutor-credit-2',
  //   [
  //     { selector: '.tutor-credit-2', directions: { x0: '-70px', y0: '0px', x1: '0', y1: '0' }, duration: .85, delay: 0 },
  //   ],
  //   { condition: r => (vh() - r.top) / vh() > .1 && r.bottom / vh() > 0, once: false, opacity: true }
  // );
  // float('.tutor-credit-1',
  //   [
  //     { selector: '.tutor-credit-1', directions: { x0: '-70px', y0: '0px', x1: '0', y1: '0' }, duration: .85, delay: 0 },
  //   ],
  //   { condition: r => (vh() - r.top) / vh() > 0 && r.bottom / vh() > .1, once: false, opacity: true }
  // );
});


function initHeader(callback = () => {}) {
  setTimeout(() => {
      document.body.classList.add('ready');
  }, 300);

  const bg = document.querySelector('section.header .image-background-wrapper');
  const bgImg = document.querySelector('section.header .image-background img');
  const src = bgImg.currentSrc || bgImg.src;
  
  onImagesReady([src], () => {
    bg.classList.add('ready');
    callback();
    document.querySelectorAll('.logo .before > span, .logo .main').forEach(b => b.classList.add('animate-in'));
  });
}


function onImagesReady(images, callback) {
  images.forEach(imgUrl => {
    const img = document.createElement('img');
    img.addEventListener('load', () => {
      // img.remove();
      incrementCounter();
    });
    img.setAttribute('src', imgUrl);
  });
  const count = images.length;
  let counter = 0;
  function incrementCounter() {
      counter++;
      if (counter === count) {
          callback();
      }
  }
}

function vh() {
  return window.innerHeight;
}