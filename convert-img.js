const fs = require('fs');
const path = require('path');
const globby = require('globby');
var im = require('imagemagick');


var targetDir = './img-magic/';
if (!fs.existsSync(targetDir)){
    fs.mkdirSync(targetDir);
}

const dir = process.argv[2] || "./img/*";

(async () => {
	const paths = await globby([dir]);
	console.log(paths);
    for (let filePath of paths) {
        processFile(path.resolve(__dirname, filePath));
    }
})();

const SIZES_SM = [
    180, 
    360, 
    540, 
    720,
];
const SIZES_MD = [
    640, 
    960, 
    1280,
];
const SIZES_MOB = [
    640, 
    820, 
    1024,
];
const SIZES_LG = [
    640, 
    960, 
    // 1024,
    1280,
    1920,
    2560,
];

const RAFAL_sizes = [ 2400, 1800, 1200, 960, 640, 320 ]
const sizes_1024 = [ 1024, 820, 640 ];


async function processFile(fileName) {   
    const extname = path.extname(fileName);
    const basename = path.basename(fileName, extname);

    im.identify(fileName, function(err, meta){
        if (err) throw err;

        let sizes;
        if (meta.width >= 2560) {
            sizes = SIZES_LG;
        } else if (meta.width >= 1280) {
            sizes = SIZES_MD;
        } else if (meta.width >= 1024) {
            sizes = SIZES_MOB;
        } else {
            sizes = SIZES_SM;
        }
        sizes = sizes_1024;
        // let sizes = SIZES_LG.slice().filter(s => s <= meta.width);

        for (let size of sizes) {
            const targetBase = path.resolve(__dirname, targetDir + basename + '-' + size);
            const targets = [
                targetBase + extname,
            ];
            if (extname === '.png') targets.push(targetBase + '.webp');
            
            targets.forEach(target => {
                im.convert([fileName, '-resize', size, '-quality', 85, target], 
                function(err, stdout){
                    if (err) throw err;
                    console.log('converted ' + target);
                });
            });
        }
    })
}