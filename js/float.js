import onVisible from './visible';

export default function initFloat(triggerSelector, targetInfos, opts) {
    // return;
    const trigger = document.querySelector(triggerSelector);
    const targets = targetInfos.map(t => document.querySelector(t.selector));
  
    targets.forEach((target, i) => {
      const { directions, duration = 1, delay = 0, ease = 'ease' } = targetInfos[i];
      requestAnimationFrame(() => {
        animateOut();
        requestAnimationFrame(() => {
          const t = `${duration}s ${delay}s ${ease}`;
          target.style.transition = `transform ${t}, opacity ${t}`;
        });
      });
      if (directions.unit == 'parent-relative') {
        targetInfos[i].relativeParent = trigger.querySelector('.background');
      }
    });
    
    if (opts.trigger === 'hover') {
      trigger.addEventListener('mouseenter', animateIn, { passive: true });
      trigger.addEventListener('mouseleave', animateOut, { passive: true });
    } else {
      setTimeout(() => {
        let listener = onVisible(trigger, animateIn, animateOut, opts);
      }, 100);
    }
  
    function parentPercentX(x1, relativeParent) {
      if (relativeParent && x1.indexOf('%%') > -1) {
        let w = relativeParent.getBoundingClientRect().width;
        x1 = parseFloat(x1);
        x1 = (x1 / 100) * w + 'px';
      }
      return x1;
    }
    function parentPercentY(x1, relativeParent) {
      if (relativeParent && x1.indexOf('%%') > -1) {
        let h = relativeParent.getBoundingClientRect().height;
        x1 = parseFloat(x1);
        x1 = (x1 / 100) * h + 'px';
      }
      return x1;
    }
    function animateIn() {
      targets.forEach((target, i) => {
        const { directions: dir, relativeParent } = targetInfos[i];
          target.classList.add('animate-in');
          target.classList.remove('animate-out');
          let { x1, y1 } = dir;
          if (relativeParent) {
            x1 = parentPercentX(x1, relativeParent);
            y1 = parentPercentY(y1, relativeParent);
          }
          target.style.transform = `translate3D(${x1},${y1},0) scale(${dir.s1 || 1})`;
          if (opts.opacity)
            target.style.opacity = `1`;
      });
    }
    function animateOut() {
      targets.forEach((target, i) => {
        const { directions: dir } = targetInfos[i];
          target.classList.remove('animate-in');
          target.classList.add('animate-out');
          target.style.transform = `translate3D(${dir.x0 || 0},${dir.y0 || 0},0) scale(${dir.s0 || 1})`;
          if (opts.opacity)
            target.style.opacity = `0`;
      });
    }
  }
  