import throttle from './throttle';

const defaultCondition = r => (vh() - r.top) / vh() > 0 && r.bottom / vh() > 0;
const defaultInvisibleCondition = r => (vh() - r.top) / vh() <= 0 || r.bottom / vh() <= 0;

export default function onVisible(node, visibleCallback, invisibleCallback, { condition = defaultCondition, once = false, ignoreInitial = false } = {}) {
  let visible = false;
  function animate() {
    // console.log('animate')
    const r = getViewportPosition(node);
    if (condition(r)) {
      if (!visible) {
        visibleCallback(r);
        visible = true;
        if (once) {
          unbind();
        }
      }
    } else {
      if (visible) {
        if (invisibleCallback) {
          invisibleCallback(r);
        }
        visible = false;
      }
    }
  }
  const _animate = throttle(animate, 200);
  function unbind() {
    document.removeEventListener('scroll', _animate, { passive: true });
  }
  if (!ignoreInitial) {
    animate();
  }
  document.addEventListener('scroll', _animate, { passive: true });

  function handleVisibilityChange() {
    if (document.visibilityState === 'visible') {
      requestAnimationFrame(() => {
        visible = true;
        // animate();
        requestAnimationFrame(() => {
          visible = false;
          animate();
        });
      });
    } else  {
    }
  }
  document.addEventListener('visibilitychange', handleVisibilityChange, false);

  return { unbind };
}


export function onVisibleInvisible(node, visibleCallback, invisibleCallback, { condition = defaultCondition, invisibleCondition = defaultInvisibleCondition, once = false } = {}) {
  let visible = false;
  function animate() {
    // console.log('animate')
    const r = getViewportPosition(node);
    if (condition(r)) {
      if (!visible) {
        visibleCallback(r);
        visible = true;
        if (once) {
          unbind();
        }
      }
    } else if (invisibleCondition(r)) {
      if (visible) {
        invisibleCallback(r);
        visible = false;
      }
    }
  }
  const _animate = throttle(animate, 200);
  function unbind() {
    document.removeEventListener('scroll', _animate, { passive: true });
  }
  document.addEventListener('scroll', _animate, { passive: true });
  animate();

  return { unbind };
}



export function onVisible_old(node, visibleCallback, invisibleCallback, { condition =  r => (vh() - r.top) / vh() > 0 && r.bottom / vh() > 0, once = false } = {}) {
  let visible = false;
  function animate() {
    // console.log('animate')
    const r = getViewportPosition(node);
    if (condition(r)) {
      if (!visible) {
        visibleCallback();
        visible = true;
        if (once) {
          unbind();
        }
      }
    } else {
      if (visible && invisibleCallback) {
        invisibleCallback();
        visible = false;
      }
    }
  }
  const _animate = throttle(animate, 200);
  function unbind() {
    document.removeEventListener('scroll', _animate, { passive: true });
  }
  animate();
  document.addEventListener('scroll', _animate, { passive: true });
  return { unbind };
}


  function getViewportPosition(element) {
    var clientRect = element.getBoundingClientRect();
    var top = clientRect.top;
    var bottom = clientRect.bottom;
    return { top, bottom };
  }


  function vh() {
    return window.innerHeight;
  }